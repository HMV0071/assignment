const bcrypt = require("bcryptjs");
const UserModel = require("../user/user.model");
const { sendError, sendSuccess } = require("../../utils/response");
const { generateJWT } = require("../../utils/auth");

class Controller {
  async register(req, res, next) {
    if (!req.body.email || !req.body.password) {
      return sendError(next, "Required fields missing", 400);
    }

    if (await UserModel.findOne({ where: { email: req.body.email } })) {
      return sendError(next, "User with email already exists", 400);
    }

    req.body.password = await bcrypt.hash(req.body.password, 10);

    let user;
    try {
      user = await UserModel.create(req.body);
    } catch (err) {
      return next(err);
    }

    user.password = undefined;
    return res.json(user);
  }

  async login(req, res, next) {
    if (!req.body.email || !req.body.password) {
      return sendError(next, "Required fields missing", 400);
    }

    const user = await UserModel.findOne({ where: { email: req.body.email } });
    if (!user) {
      return sendError(next, "User does not exist", 404);
    }
    console.log(user.password);
    if (!(await bcrypt.compare(req.body.password, user.password))) {
      return sendError(next, "Wrong password", 401);
    }

    const token = generateJWT({ id: user.id, role: user.role });

    user.password = undefined;
    return sendSuccess(res, {
      token,
      account: user,
    });
  }
}

module.exports = new Controller();
