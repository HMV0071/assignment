const bcrypt = require("bcryptjs");
const Sequelize = require("sequelize");
const db = require("../../config/database.config");
const { roles } = require("../../config/roles");

const User = db.define("user", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  username: Sequelize.STRING,
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  pic: Sequelize.STRING,
  address: Sequelize.STRING,
  password: {
    type: Sequelize.STRING,
    // set(value) {
    //   console.log(value);
    //   this.setDataValue("password", bcrypt.hashSync(value, 10));
    // },
  },
  phone: Sequelize.STRING,
  city: Sequelize.STRING,
  country: Sequelize.STRING,
  post_code: Sequelize.STRING,
  position: Sequelize.STRING,
  role: {
    type: Sequelize.ENUM,
    values: roles,
    defaultValue: roles[0],
  },
  active: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
});

User.sync({ force: true });

module.exports = User;
