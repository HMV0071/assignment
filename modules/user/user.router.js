const { Router } = require("express");
const { checkAuth } = require("../../middlewares/auth");
const controller = require("./user.controller");
const router = Router();

router.route("/me").get(checkAuth, controller.me);

module.exports = router;
