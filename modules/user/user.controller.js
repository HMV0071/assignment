const { sendSuccess } = require("../../utils/response");
const UserModel = require("./user.model");

class Controller {
  async me(req, res, next) {
    const user = await UserModel.findByPk(req.user.id);
    user.password = undefined;
    return sendSuccess(res, user);
  }
}

module.exports = new Controller();
