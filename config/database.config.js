const Sequelize = require("sequelize");

module.exports = new Sequelize("nodemysql", "root", "", {
  // process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD
  host: "localhost", // process.env.DB_HOST
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});
