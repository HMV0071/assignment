const { Router } = require("express");
const router = Router();

const authRouter = require("./modules/auth/auth.router");
const userRouter = require("./modules/user/user.router");

router.use("/auth", authRouter);
router.use("/users", userRouter);

module.exports = router;
