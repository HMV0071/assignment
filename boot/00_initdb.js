const db = require("../config/database.config");

module.exports = async () => {
  return new Promise((resolve, reject) => {
    db.authenticate()
      .then(() => {
        console.log("DB connected!");
        resolve();
      })
      .catch((err) => {
        reject(err);
      });
  });
};
